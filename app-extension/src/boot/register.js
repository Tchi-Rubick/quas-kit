import { boot } from 'quasar/wrappers'
import VuePlugin from 'quasar-ui-quas-kit'

export default boot(({ app }) => {
  app.use(VuePlugin)
})
