<img src="https://img.shields.io/npm/v/quasar-ui-quas-kit.svg?label=quasar-ui-quas-kit">
<img src="https://img.shields.io/npm/v/quasar-app-extension-quas-kit.svg?label=quasar-app-extension-quas-kit">

Compatible with Quasar UI v2 and Vue 3.

# Structure
* [/ui](ui) - standalone npm package
* [/app-extension](app-extension) - Quasar app extension

# Donate
If you appreciate the work that went into this project, please consider [donating to Quasar](https://donate.quasar.dev).

# License
MIT (c) Ritchi
